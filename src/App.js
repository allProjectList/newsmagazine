import React from 'react';
import './App.css';
import Trading from './includes/trading/trading'
import Header from './includes/header/header'
import Footer from './includes/footer/footer'
import Nav from './includes/nav/nav'
import Home from './pages/home'

function App() {
  return (
    <div class="body-inner">
      <Trading/>
      <Header/>
      <Nav/>
      <Home/>
      <Footer/>
    </div>
  );
}

export default App;
