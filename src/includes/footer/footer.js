import React from 'react';

function Footer(){
    return(
        <div>
            <footer id="footer" className="footer">
                <div className="footer-main">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-3 col-sm-12 footer-widget">
                                <h3 className="widget-title">Trending Now</h3>
                                <div className="list-post-block">
                                    <ul className="list-post">
                                        <li className="clearfix">
                                            <div className="post-block-style post-float clearfix">
                                                <div className="post-thumb">
                                                    <a href="#">
                                                        <img className="img-fluid" src="images/news/lifestyle/health2.jpg" alt="" />
                                                    </a>
                                                </div>

                                                <div className="post-content">
                                                    <h2 className="post-title title-small">
                                                        <a href="#">Can't shed those Gym? The problem might...</a>
                                                    </h2>
                                                    <div className="post-meta">
                                                        <span className="post-date">Mar 13, 2017</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>

                                        <li className="clearfix">
                                            <div className="post-block-style post-float clearfix">
                                                <div className="post-thumb">
                                                    <a href="#">
                                                        <img className="img-fluid" src="images/news/lifestyle/health3.jpg" alt="" />
                                                    </a>
                                                </div>

                                                <div className="post-content">
                                                    <h2 className="post-title title-small">
                                                        <a href="#">Deleting fears from the brain means you…</a>
                                                    </h2>
                                                    <div className="post-meta">
                                                        <span className="post-date">Jan 11, 2017</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>

                                        <li className="clearfix">
                                            <div className="post-block-style post-float clearfix">
                                                <div className="post-thumb">
                                                    <a href="#">
                                                        <img className="img-fluid" src="images/news/lifestyle/health4.jpg" alt="" />
                                                    </a>
                                                </div>

                                                <div className="post-content">
                                                    <h2 className="post-title title-small">
                                                        <a href="#">Smart packs parking sensor tech...</a>
                                                    </h2>
                                                    <div className="post-meta">
                                                        <span className="post-date">Feb 19, 2017</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                
                            </div>

                            <div className="col-lg-3 col-sm-12 footer-widget widget-categories">
                                <h3 className="widget-title">Hot Categories</h3>
                                <ul>
                                    <li>
                                        <a href="#"><span className="catTitle">Robotics</span><span className="catCounter"> (5)</span></a>
                                    </li>
                                    <li>
                                        <a href="#"><span className="catTitle">Games</span><span className="catCounter"> (6)</span></a>
                                    </li>
                                    <li>
                                        <a href="#"><span className="catTitle">Gadgets</span><span className="catCounter"> (5)</span></a>
                                    </li>
                                    <li>
                                        <a href="#"><span className="catTitle">Travel</span><span className="catCounter"> (5)</span></a>
                                    </li>
                                    <li>
                                        <a href="#"><span className="catTitle">Health</span><span className="catCounter"> (5)</span></a>
                                    </li>
                                    <li>
                                        <a href="#"><span className="catTitle">Architecture</span><span className="catCounter"> (5)</span></a>
                                    </li>
                                    <li>
                                        <a href="#"><span className="catTitle">Food</span><span className="catCounter"> (5)</span></a>
                                    </li>
                                </ul>
                                
                            </div>

                            <div className="col-lg-3 col-sm-12 footer-widget twitter-widget">
                                <h3 className="widget-title">Latest Tweets</h3>
                                <ul>
                                    <li>
                                        <div className="tweet-text">
                                        <span>About 13 days ago</span>
                                        Please, Wait for the next version of our templates to update #Joomla 3.7 
                                        <a href="#">https://t.co/LlEv8HgokN</a>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="tweet-text">
                                        <span>About 15 days ago</span>
                                        #WordPress 4.8 is here!
                                        <a href="#">https://t.co/uDjRH4Gya9</a>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="tweet-text">
                                        <span>About 1 month ago</span>
                                        Please, Wait for the next version of our templates to update #Joomla 3.7 
                                        <a href="#">https://t.co/LlEv8HgokN</a>
                                        </div>
                                    </li>
                                </ul>
                            </div>

                            <div className="col-lg-3 col-sm-12 footer-widget">
                                <h3 className="widget-title">Post Gallery</h3>
                                <div className="gallery-widget">
                                    <a href="#"><img className="img-fluid" src="images/news/lifestyle/health1.jpg" alt="" /></a>
                                    <a href="#"><img className="img-fluid" src="images/news/lifestyle/food3.jpg" alt="" /></a>
                                    <a href="#"><img className="img-fluid" src="images/news/lifestyle/travel4.jpg" alt="" /></a>
                                    <a href="#"><img className="img-fluid" src="images/news/lifestyle/architecture1.jpg" alt="" /></a>
                                    <a href="#"><img className="img-fluid" src="images/news/tech/gadget1.jpg" alt="" /></a>
                                    <a href="#"><img className="img-fluid" src="images/news/tech/gadget2.jpg" alt="" /></a>
                                    <a href="#"><img className="img-fluid" src="images/news/tech/game2.jpg" alt="" /></a>
                                    <a href="#"><img className="img-fluid" src="images/news/tech/robot5.jpg" alt="" /></a>
                                    <a href="#"><img className="img-fluid" src="images/news/lifestyle/travel5.jpg" alt="" /></a>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                <div className="footer-info text-center">
                    <div className="container">
                        <div className="row">
                            <div className="col-md-12">
                                <div className="footer-info-content">
                                    <div className="footer-logo">
                                        <img className="img-fluid" src="images/logos/footer-logo.png" alt="" />
                                    </div>
                                    <p>News247 Worldwide is a popular online newsportal and going source for technical and digital content for its influential audience around the globe. You can reach us via email or phone.</p>
                                    <p className="footer-info-phone"><i className="fa fa-phone"></i> +(785) 238-4131</p>
                                    <p className="footer-info-email"><i className="fa fa-envelope-o"></i> editor@news247.com</p>
                                    <ul className="unstyled footer-social">
                                        <li>
                                            <a title="Rss" href="#">
                                                <span className="social-icon"><i className="fa fa-rss"></i></span>
                                            </a>
                                            <a title="Facebook" href="#">
                                                <span className="social-icon"><i className="fa fa-facebook"></i></span>
                                            </a>
                                            <a title="Twitter" href="#">
                                                <span className="social-icon"><i className="fa fa-twitter"></i></span>
                                            </a>
                                            <a title="Google+" href="#">
                                                <span className="social-icon"><i className="fa fa-google-plus"></i></span>
                                            </a>
                                            <a title="Linkdin" href="#">
                                                <span className="social-icon"><i className="fa fa-linkedin"></i></span>
                                            </a>
                                            <a title="Skype" href="#">
                                                <span className="social-icon"><i className="fa fa-skype"></i></span>
                                            </a>
                                            <a title="Skype" href="#">
                                                <span className="social-icon"><i className="fa fa-dribbble"></i></span>
                                            </a>
                                            <a title="Skype" href="#">
                                                <span className="social-icon"><i className="fa fa-pinterest"></i></span>
                                            </a>
                                            <a title="Skype" href="#">
                                                <span className="social-icon"><i className="fa fa-instagram"></i></span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </footer>

            <div className="copyright">
                <div className="container">
                    <div className="row">
                        <div className="col-sm-12 col-md-6">
                            <div className="copyright-info">
                                <span>Copyright © 2018 News247 All Rights Reserved. Designed By Tripples</span>
                            </div>
                        </div>

                        <div className="col-sm-12 col-md-6">
                            <div className="footer-menu">
                                <ul className="nav unstyled">
                                    <li><a href="#">Site Terms</a></li>
                                    <li><a href="#">Privacy</a></li>
                                    <li><a href="#">Advertisement</a></li>
                                    <li><a href="#">Cookies Policy</a></li>
                                    <li><a href="#">Contact Us</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div id="back-to-top" className="back-to-top">
                        <button className="btn btn-primary" title="Back to Top">
                            <i className="fa fa-angle-up"></i>
                        </button>
                    </div>

                </div>
            </div> 
        </div> 

    );
};

export default Footer;