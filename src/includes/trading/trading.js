import React from 'react';

function Trading(){
	return(
		<div className="trending-bar d-md-block d-lg-block d-none">
			<div className="container">
				<div className="row">
					<div className="col-md-12">
						<h3 className="trending-title">Trending Now</h3>
						<div id="trending-slide" className="owl-carousel owl-theme trending-slide">
							<div className="item">
							<div className="post-content">
								<h2 className="post-title title-small">
									<a href="#">The best MacBook Pro alternatives in 2017 for Apple users</a>
								</h2>
							</div>
							</div>
							<div className="item">
							<div className="post-content">
								<h2 className="post-title title-small">
									<a href="#">Soaring through Southern Patagonia with the Premium Byrd drone</a>
								</h2>
							</div>
							</div>
							<div className="item">
							<div className="post-content">
								<h2 className="post-title title-small">
									<a href="#">Super Tario Run isn’t groundbreaking, but it has Mintendo charm</a>
								</h2>
							</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	)
}

export default Trading;